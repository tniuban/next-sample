import React from 'react';
import NotFound from '@apps/404';

export default function PageNotFound() {
  return <NotFound />;
}
