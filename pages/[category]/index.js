import React from 'react';
// import config from 'config.js';
import ProductList from '@apps/Product/List';

export async function getStaticProps({ params }) {
  // const res = await fetch(`${config.domain}/api/category/find-categories`);
  // const categories = await res.json().then((resp) => resp.data.categories);
  return {
    props: { roleIds: ['root'], needSignIn: false, params },
  };
}
export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking',
  };
}

export default function Category({ params }) {
  return <ProductList categoryId={params.category} />;
}
