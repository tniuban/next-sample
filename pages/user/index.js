import React from 'react';
import UserList from '@apps/User/List';

export default function UserListPage() {
  return <UserList />;
}

export async function getServerSideProps() {
  return {
    props: { roleIds: ['rodot'] },
  };
}
