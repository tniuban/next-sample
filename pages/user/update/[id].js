import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import UserForm from '@apps/User/Form';
import Error from '@components/Errror';
import Loading from '@components/Loading';
import { fetcher } from '@services';

export default function UpdateUserPage() {
  const router = useRouter();
  const { id } = router.query;
  const [initialValues, setInitialValues] = useState({});
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState();

  useEffect(() => {
    const fetchUserDetail = async (id) => {
      setLoading(true);
      try {
        const data = await fetcher(`/users/${id}`);
        setInitialValues(data.data);
      } catch (error) {
        setError(error.message);
      }
      setLoading(false);
    };
    if (id) {
      fetchUserDetail(id);
    }
  }, [id]);

  if (loading) {
    return <Loading />;
  }

  if (error) {
    return (
      <div className='container-md'>
        <Error message={error} />;
      </div>
    );
  }

  return <UserForm initialValues={initialValues} />;
}

export async function getServerSideProps() {
  return {
    props: { roleIds: ['root'] },
  };
}
