import React from 'react';
import UserForm from '@apps/User/Form';

export default function CreateUserPage() {
  return <UserForm />;
}

export async function getServerSideProps() {
  return {
    props: { roleIds: ['root'] },
  };
}
