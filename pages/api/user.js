import { catchErrorsFrom, resp, withSession } from '@services';

const handler = withSession((req, res) => {
  const userId = req.session.get('userId');
  if (!userId) {
    return resp({ res, status: true, data: { isLoggedIn: false } });
  }
  const details = req.session.get(`${userId}_details`);
  return resp({ res, status: true, data: { isLoggedIn: true, details } });
}, true);
export default catchErrorsFrom(handler);
