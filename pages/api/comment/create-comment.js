import { withSession, resp, catchErrorsFrom } from '@services';
import { createComment } from '@data/comment';

const handler = withSession(async (req, res) => {
  const { productId, name, description, rate } = req.body;
  const params = {
    productId,
    name,
    description,
    rate,
  };
  const data = await createComment(params);

  return resp({
    res,
    status: true,
    data: data,
  });
});

export default catchErrorsFrom(handler);
