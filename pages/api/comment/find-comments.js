import { withSession, resp, catchErrorsFrom } from '@services';
import { findComments } from '@data/comment';

const handler = withSession(async (req, res) => {
  const { productId, limit, offset } = req.body;
  const params = {
    productId,
    limit,
    offset,
  };
  const data = await findComments(params);

  return resp({
    res,
    status: true,
    data: data,
  });
});

export default catchErrorsFrom(handler);
