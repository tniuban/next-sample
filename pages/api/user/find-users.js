import { withSession, resp, catchErrorsFrom } from '@services';
import { findUsers } from '@data/user';

const handler = withSession(async (req, res) => {
  const { limit, offset } = req.body;
  const params = {
    limit,
    offset,
  };
  const data = await findUsers(params);

  return resp({
    res,
    status: true,
    data: data,
  });
});

export default catchErrorsFrom(handler);
