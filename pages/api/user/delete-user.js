import { withSession, resp, catchErrorsFrom } from '@services';
import { deleteUser } from '@data/user';

const handler = withSession(async (req, res) => {
  const { id } = req.body;
  const params = { id };
  const data = await deleteUser(params);

  return resp({
    res,
    status: true,
    data: data,
  });
});

export default catchErrorsFrom(handler);
