import { withSession, resp, catchErrorsFrom } from '@services';
import { updateUser } from '@data/user';

const handler = withSession(async (req, res) => {
  const {
    id,
    username,
    password,
    firstName,
    lastName,
    maidenName,
    gender,
    email,
    phone,
    birthDate,
    age,
    adress,
  } = req.body;
  const params = {
    id,
    username,
    password,
    firstName,
    lastName,
    maidenName,
    gender,
    email,
    phone,
    birthDate,
    age,
    adress,
  };
  const data = await updateUser(params);

  return resp({
    res,
    status: true,
    data: data,
  });
});

export default catchErrorsFrom(handler);
