import { withSession, resp, catchErrorsFrom } from '@services';
import { createUser } from '@data/user';

const handler = withSession(async (req, res) => {
  const {
    username,
    password,
    firstName,
    lastName,
    maidenName,
    gender,
    email,
    phone,
    birthDate,
    age,
    adress,
  } = req.body;
  const params = {
    username,
    password,
    firstName,
    lastName,
    maidenName,
    gender,
    email,
    phone,
    birthDate,
    age,
    adress,
  };
  const data = await createUser(params);

  return resp({
    res,
    status: true,
    data: data,
  });
});

export default catchErrorsFrom(handler);
