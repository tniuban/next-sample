import { findBrands } from '@data/brand';
import { withSession, resp, catchErrorsFrom } from '@services';

const handler = withSession(async (req, res) => {
  const { categoryIds, search, limit, offset } = req.body;
  const params = { categoryIds, search, limit, offset };
  const data = await findBrands(params);

  return resp({
    res,
    status: true,
    data: data,
  });
});

export default catchErrorsFrom(handler);
