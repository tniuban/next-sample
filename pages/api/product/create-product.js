import { withSession, resp, catchErrorsFrom } from '@services';
import { createProduct } from '@data/product';

const handler = withSession(async (req, res) => {
  const {
    name,
    brandId,
    stock,
    price,
    discountPrice,
    thumbnail,
    rate,
    totalComment,
    giftPrice,
    details,
    categoryId,
  } = req.body;
  const params = {
    name,
    brandId,
    stock,
    price,
    discountPrice,
    thumbnail,
    rate,
    totalComment,
    giftPrice,
    details,
    categoryId,
  };
  const data = await createProduct(params);

  return resp({
    res,
    status: true,
    data: data,
  });
});

export default catchErrorsFrom(handler);
