import { withSession, resp, catchErrorsFrom } from '@services';
import { findProducts } from '@data/product';

const handler = withSession(async (req, res) => {
  const { categoryId, brandId, search, limit, offset } = req.body;
  const params = {
    categoryId,
    brandId,
    search,
    limit,
    offset,
  };
  const data = await findProducts(params);

  return resp({
    res,
    status: true,
    data: data,
  });
}, true);

export default catchErrorsFrom(handler);
