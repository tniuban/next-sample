import { withSession, resp, catchErrorsFrom } from '@services';
import { updateProduct } from '@data/product';

const handler = withSession(async (req, res) => {
  const {
    id,
    name,
    brand,
    stock,
    price,
    discountPrice,
    thumbnail,
    rate,
    totalComment,
    giftPrice,
    details,
    category,
  } = req.body;
  const params = {
    id,
    name,
    brand,
    stock,
    price,
    discountPrice,
    thumbnail,
    rate,
    totalComment,
    giftPrice,
    details,
    category,
  };
  const data = await updateProduct(params);

  return resp({
    res,
    status: true,
    data: data,
  });
});

export default catchErrorsFrom(handler);
