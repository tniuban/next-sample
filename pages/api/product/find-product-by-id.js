import { withSession, resp, catchErrorsFrom } from '@services';
import { findProductById } from '@data/product';

const handler = withSession(async (req, res) => {
  const { id } = req.body;
  const params = { id };

  const data = await findProductById(params);

  return resp({
    res,
    status: true,
    data: data,
  });
});

export default catchErrorsFrom(handler);
