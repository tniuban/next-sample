import { findCategories } from '@data/category';
import { withSession, resp, catchErrorsFrom } from '@services';

const handler = withSession(async (req, res) => {
  const { limit, offset } = req.body;
  const params = {
    limit,
    offset,
  };
  const data = await findCategories(params);
  console.log('data', data);
  return resp({
    res,
    status: true,
    data: data,
  });
}, true);

export default catchErrorsFrom(handler);
