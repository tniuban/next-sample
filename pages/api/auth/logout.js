import { withSession, resp, catchErrorsFrom } from '@services';

const handler = withSession(async (req, res) => {
  req.session.destroy();
  return resp({
    res,
    status: true,
    data: { isLoggedIn: false },
  });
}, true);

export default catchErrorsFrom(handler);
