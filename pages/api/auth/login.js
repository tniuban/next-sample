import { withSession, resp, catchErrorsFrom } from '@services';
import { login } from 'src/data/auth';

const handler = withSession(async (req, res) => {
  const { username, password } = req.body;
  const params = {
    username,
    password,
  };
  const data = await login(params);
  const { id, token, refreshToken, ...others } = data;
  const userDetails = { id, ...others };

  req.session.set('userId', id);
  req.session.set(`${id}_details`, userDetails);
  req.session.set(`${id}_token`, token);
  req.session.set(`${id}_refreshToken`, refreshToken);
  await req.session.save();

  return resp({
    res,
    status: true,
    data: userDetails,
  });
}, true);

export default catchErrorsFrom(handler);
