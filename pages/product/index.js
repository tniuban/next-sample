import React from 'react';
import ProductList from '@apps/Product/List';

export default function ProductListPage() {
  return <ProductList />;
}
