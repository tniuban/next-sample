import React from 'react';
import ProductForm from '@apps/Product/Form';

export default function CreateProductPage() {
  return <ProductForm />;
}

export async function getServerSideProps() {
  return {
    props: { roleIds: ['root'] },
  };
}
