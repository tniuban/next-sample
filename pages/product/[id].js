import React, { useRef } from 'react';
import { useRouter } from 'next/router';
import { Comments, ProductTechDetails } from '@apps/Product/Details';

export default function ProductDetailsPage() {
  const router = useRouter();
  const { id } = router.query;
  const commentRef = useRef(null);
  return (
    <div className="container" style={{ maxWidth: 800 }}>
      <ProductTechDetails id={id} commentRef={commentRef} />
      <section ref={commentRef}>
        <Comments productId={id} />
      </section>
    </div>
  );
}
