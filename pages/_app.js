import '../styles/globals.scss';
import React, { useCallback, useEffect, useState } from 'react';
import { store } from 'src/redux/store';
import Toast from '@apps/Toast';
import Head from 'next/head';
import AppsWrapper from '@apps/AppsWrapper';
import { useRouter } from 'next/router';
import Loader from '@components/Loader';
import LayoutLogin from '@components/LayoutLogin';
import Login from '@apps/Login';
import { Provider } from 'react-redux';
import { useUser } from '@helpers/user';
import NoPermission from '@apps/NoPermission';
import Navbar from '@components/Navbar';
import useSWR from 'swr';
import { poster } from '@services';

const AuthenticateComponent = ({ Component, pageProps }) => {
  const { user } = useUser();
  const { data: resp } = useSWR('/api/category/find-categories', poster);
  const hasPermission =
    Array.isArray(pageProps.roleIds) && pageProps.roleIds.length > 0;
  if (!pageProps.needSignIn)
    return (
      <>
        <Navbar categories={resp?.data?.data?.categories || []} />
        <Component {...pageProps} />
      </>
    );
  if (!user) return <></>;
  if (!user.isLoggedIn) {
    return (
      <LayoutLogin>
        <Login />
      </LayoutLogin>
    );
  }
  const permissionRequired = hasPermission;
  pageProps.roleIds.some((role) => !user.details?.roleIds?.includes(role));
  return (
    <AppsWrapper>
      {permissionRequired ? <NoPermission /> : <Component {...pageProps} />}
    </AppsWrapper>
  );
};

function MyApp({ Component, pageProps }) {
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const handleRouteChange = useCallback(
    (_state) => () => {
      setLoading(_state);
    },
    []
  );

  useEffect(() => {
    router.events.on('routeChangeStart', handleRouteChange(true));
    router.events.on('routeChangeComplete', handleRouteChange(false));
    router.events.on('routeChangeError', handleRouteChange(false));
    return () => {
      router.events.off('routeChangeStart', handleRouteChange(true));
      router.events.off('routeChangeComplete', handleRouteChange(false));
      router.events.off('routeChangeError', handleRouteChange(false));
    };
  }, [router, handleRouteChange]);

  return (
    <Provider store={store}>
      {loading && <Loader />}
      <Head>
        <title>Loading</title>
      </Head>
      <Toast />
      <AuthenticateComponent Component={Component} pageProps={pageProps} />
    </Provider>
  );
}

export default MyApp;
