const headerConfig = [
  { href: '/', label: 'Home' },
  { href: '/user', label: 'User' },
  { href: '/user/create', label: 'User' },
];

export default headerConfig;
