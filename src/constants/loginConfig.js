import { SecondaryInput } from '@components/Input';
import { validateHelpers } from '@helpers/form';

const loginConfig = [
  {
    key: 'username',
    hidden: false,
    Component: SecondaryInput,
    required: true,
    disabled: false,
    defaultValue: '',
    placeholder: 'Username or Your email address',
    validation: [
      validateHelpers.string(),
      validateHelpers.required('Username is required'),
    ],
  },
  {
    key: 'password',
    hidden: false,
    Component: SecondaryInput,
    required: true,
    disabled: false,
    defaultValue: '',
    placeholder: 'Password',
    validation: [
      validateHelpers.string(),
      validateHelpers.required('Password is required'),
    ],
  },
];

export default loginConfig;
