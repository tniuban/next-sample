const toastType = {
  SUCCESS: 'success',
  ERROR: 'error',
  INFO: 'info',
  WARNING: 'warning',
};

const toastBackground = {
  [toastType.SUCCESS]: '#5CB85C',
  [toastType.ERROR]: '#D9534F',
  [toastType.INFO]: '#5BC0DE',
  [toastType.WARNING]: '#F0AD4E',
};

const toastTitle = {
  [toastType.SUCCESS]: 'Success',
  [toastType.ERROR]: 'Error',
  [toastType.INFO]: 'Info',
  [toastType.WARNING]: 'Warning',
};

export { toastBackground, toastTitle, toastType };
