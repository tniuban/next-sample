const sidebarConfig = [
  {
    icon: <i className='fa-solid fa-house' />,
    href: '/',
    title: 'Home',
  },
  {
    icon: <i className='fa-solid fa-user-astronaut' />,
    href: '/user',
    title: 'User management',
  },
  {
    icon: '/',
    href: '/product',
    title: 'Product management',
  },
];

export default sidebarConfig;
