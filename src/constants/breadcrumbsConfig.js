const breadcrumbConfig = {
  index: 'Home',
  user: {
    index: 'User list',
    create: 'Create new user',
    update: {
      '[id]': 'Update user',
    },
  },
  cart: {
    index: 'Cart'
  },
  product: {
    index: 'Product list',
    create: 'Create new product',
    update: {
      '[id]': 'Update product',
    },
  },
};

export default breadcrumbConfig;
