import { PrimaryInput } from '@components/Input';
import { SelectWithOptions } from '@components/Select';
import { validateHelpers } from '@helpers/form';
import { genders } from './genders';

/**
 * Element of formConfig:
 * 1. key: field name
 * 2. path: path to field name, eg: data = {a: {b: 1}} key:b >> path ["a"]
 * 3. defaultVaulue,
 * 4. hidden.
 * 5. disabled.
 * 6. required
 * 7. validation: array of vadition.
 * 8. className: class for element.
 * 9. label.
 * 10. placeholder.
 * 11. Component: which is used for rendering.
 * 12. option: required when type = 'select', option of select.
 */

export const formConfig = [
  {
    key: 'id',
    defaultValue: '',
    hidden: true,
    validation: [],
  },
  {
    key: 'username',
    defaultValue: '',
    hidden: false,
    Component: PrimaryInput,
    label: 'Username',
    type: 'text',
    disabled: false,
    required: true,
    validation: [
      validateHelpers.string(),
      validateHelpers.required('Username is required'),
    ],
  },
  {
    key: 'password',
    defaultValue: '',
    hidden: false,
    Component: PrimaryInput,
    label: 'Password',
    type: 'text',
    disabled: false,
    required: true,
    validation: [
      validateHelpers.string(),
      validateHelpers.required('Password is required'),
    ],
  },
  {
    key: 'firstName',
    defaultValue: '',
    hidden: false,
    Component: PrimaryInput,
    label: 'First name',
    type: 'text',
    disabled: false,
    required: true,
    validation: [
      validateHelpers.string(),
      validateHelpers.required('Firstname is required'),
    ],
  },
  {
    key: 'lastName',
    defaultValue: '',
    hidden: false,
    Component: PrimaryInput,
    label: 'Last name',
    type: 'text',
    disabled: false,
    required: true,
    validation: [
      validateHelpers.string(),
      validateHelpers.required('Lastname is required'),
    ],
  },
  {
    key: 'maidenName',
    defaultValue: '',
    hidden: false,
    Component: PrimaryInput,
    label: 'Maiden name',
    type: 'text',
    disabled: false,
    required: false,
    validation: [validateHelpers.string()],
  },
  {
    key: 'gender',
    defaultValue: '',
    hidden: false,
    Component: SelectWithOptions,
    label: 'Gender',
    option: genders,
    disabled: false,
    required: true,
    validation: [
      validateHelpers.string(),
      validateHelpers.required('Gender is required'),
    ],
  },
  {
    key: 'email',
    defaultValue: '',
    hidden: false,
    Component: PrimaryInput,
    label: 'Email',
    type: 'text',
    disabled: false,
    required: true,
    validation: [
      validateHelpers.string(),
      validateHelpers.email(),
      validateHelpers.required('Email is required'),
    ],
  },
  {
    key: 'phone',
    defaultValue: '',
    hidden: false,
    Component: PrimaryInput,
    label: 'Phone number',
    type: 'text',
    disabled: false,
    required: true,
    validation: [
      validateHelpers.string(),
      validateHelpers.phone(),
      validateHelpers.required('Phone number is required'),
    ],
  },
  {
    key: 'birthDate',
    defaultValue: '',
    hidden: false,
    Component: PrimaryInput,
    label: 'Birth date',
    type: 'date',
    disabled: false,
    required: true,
    className: 'col-7 col-sm-9',
    validation: [
      validateHelpers.date(),
      validateHelpers.required('Birth date is required'),
    ],
  },
  {
    key: 'age',
    defaultValue: 0,
    hidden: false,
    Component: PrimaryInput,
    label: 'Age',
    type: 'number',
    disabled: false,
    required: true,
    className: 'col-5 col-sm-3',
    validation: [
      validateHelpers.number(),
      validateHelpers.moreThan(0),
      validateHelpers.required('Age is required'),
    ],
  },
  {
    key: 'address',
    path: ['address'],
    defaultValue: '',
    Component: PrimaryInput,
    hidden: false,
    label: 'Address line',
    type: 'text',
    disabled: false,
    required: false,
    className: 'col-12',
    validation: [validateHelpers.string()],
  },
  {
    key: 'city',
    path: ['address'],
    defaultValue: '',
    hidden: false,
    Component: PrimaryInput,
    label: 'City',
    type: 'text',
    disabled: false,
    required: false,
    className: 'col-12 col-sm-6',
    validation: [validateHelpers.string()],
  },
  {
    key: 'postalCode',
    path: ['address'],
    defaultValue: '',
    hidden: false,
    Component: PrimaryInput,
    label: 'Postal code',
    type: 'text',
    disabled: false,
    required: false,
    className: 'col-6 col-sm-3',
    validation: [validateHelpers.string()],
  },
  {
    key: 'state',
    path: ['address'],
    defaultValue: '',
    Component: PrimaryInput,
    hidden: false,
    label: 'State',
    type: 'text',
    disabled: false,
    required: false,
    className: 'col-6 col-sm-3',
    validation: [validateHelpers.string()],
  },
];
