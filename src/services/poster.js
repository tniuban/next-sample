import axios from "axios";

export default function poster(_url, _params) {
  return axios.post(_url, _params);
}
