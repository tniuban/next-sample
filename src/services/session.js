import { withIronSession } from 'next-iron-session';
import api from './api';
import resp from './resp';
import appConfig from '../../config';

export default function withSession(handler, isPublic = false) {
  return withIronSession(
    async (req, res) => {
      if (!isPublic) {
        const userId = req.session.get('userId');
        if (!userId) {
          req.session.destroy();
          return resp({
            res,
            status: false,
            code: 2001,
            error: 'Unauthorized (2001)',
          });
        }

        const token = req.session.get(`${userId}_token`);
        const refreshToken = req.session.get(`${userId}_refreshToken`);
        api.defaults.headers.uuid = userId;
        api.defaults.headers.token = token;
        api.defaults.headers.refreshToken = refreshToken;
      }

      return handler(req, res);
    },
    {
      cookieName: appConfig.sessionName,
      password: appConfig.sessionPassword,
    }
  );
}
