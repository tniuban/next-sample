import resp from './resp';

export default function catchErrorsFrom(handler) {
  return async (req, res) =>
    handler(req, res).catch((error) => {
      const stack = error.stack?.split('\n').map((line) => line.trim());
      return resp({
        res,
        code: error.code,
        status: false,
        message: `${error.message} (${error.code})`,
        debugMessage: [...[error.debugMessage], ...(stack || [])],
      });
    });
}
