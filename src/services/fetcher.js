import axios from 'axios';

export default function fetcher(_url, _params) {
  return axios.get(_url, { params: _params });
}
