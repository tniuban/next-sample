import axios from 'axios';

export default function putter(_url, _body) {
  return axios.put(_url, _body);
}
