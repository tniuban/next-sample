import axios from 'axios';

export default function deleter(_url, _params) {
  return axios.delete(_url, { params: _params });
}
