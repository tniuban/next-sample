import axios from 'axios';
import appConfig from '../../config';

const api = axios.create({
  baseURL: appConfig.baseApiUrl,
  timeout: 5 * 60 * 1000, // 5 minutes
  headers: {
    Accept: 'application/json, text/plain, */*',
    'Content-Type': 'application/json',
  },
});

api.interceptors.request.use(async (config) => {
  const { token } = api.defaults.headers;
  config.headers.Authorization = `Bearer ${token}`;
  config.headers.start = new Date();
  return config;
});
api.interceptors.response.use(
  async (response) => {
    return response;
  },
  async (error) => {
    if (error.response?.status === 401) return error.response;
    return Promise.reject(error);
  }
);
export default api;
