// eslint-disable-next-line import/no-anonymous-default-export
export default ({
  res,
  status = false,
  code = 0,
  data = [],
  error = '',
  message = '',
  debugMessage = '',
}) => res.json({ status, code, data, error, message, debugMessage });
