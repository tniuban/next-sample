class SugarError extends Error {
  constructor(code = '1000', message = '', debugMessage = '', ...params) {
    super(...params);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, SugarError);
    }

    this.code = code;
    this.message = message;
    this.error = message;
    this.debugMessage = debugMessage;
  }
}

module.exports = SugarError;
