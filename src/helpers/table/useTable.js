import { poster } from '@services';
import { useEffect, useState } from 'react';
import useSWR from 'swr';

export default function useTabe({
  limit = 10,
  skip = 0,
  keyData,
  api,
  query = {},
}) {
  const [error, setError] = useState();
  const [nextPage, setNextPage] = useState(null);
  const [prePage, setPrePage] = useState(null);
  const [paging, setPaging] = useState({
    limit,
    skip,
  });
  const [data, setData] = useState([]);
  const {
    data: resp,
    mutate,
    error: errMsg,
  } = useSWR([api, query, paging], (_url, _query, _paging) =>
    poster(_url, { limit: _paging.limit, skip: _paging.skip, ..._query })
  );

  const loading = !resp && !error;

  useEffect(() => {
    if (resp && resp.data && !resp.data.code) {
      const dt = resp.data.data;
      setData(dt[keyData]);
      setNextPage(dt.nextPage);
      setPrePage(skip >= limit ? { limit, skip: skip - limit } : null);
      return;
    }
    setError(errMsg || resp?.data.message);
  }, [resp, errMsg, keyData, limit, skip]);

  const handlePagination = (_paging) => setPaging(_paging);

  return {
    loading,
    error,
    data,
    mutate,
    limit,
    setLimit: (_limit) => setPaging((pre) => ({ ...pre, limit: _limit })),
    skip,
    setSkip: (_skip) => setPaging((pre) => ({ ...pre, skip: _skip })),
    nextPage,
    prePage,
    handlePagination,
  };
}
