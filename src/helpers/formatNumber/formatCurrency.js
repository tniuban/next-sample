const formatCurrency = (_value, _currency = 'VND', _locales = 'vi-VN') =>
  new Intl.NumberFormat(_locales, {
    style: 'currency',
    currency: _currency,
  }).format(_value);

export default formatCurrency;
