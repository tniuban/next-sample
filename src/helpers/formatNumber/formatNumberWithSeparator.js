export default function formatNumberWithSeparator(
  _number,
  _minimumFractionDigits = 0,
  _maximumFractionDigits = 2
) {
  if (isNaN(_number)) return undefined;
  const formatedNumber = Number(_number).toLocaleString('us-US', {
    minimumFractionDigits: _minimumFractionDigits,
    maximumFractionDigits: _maximumFractionDigits,
  });
  return formatedNumber;
}
