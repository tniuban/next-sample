const getDimensions = (ele) => {
  if (!ele) return {};
  const { width, height } = ele.getBoundingClientRect();
  const { offsetTop } = ele;
  const offsetBottom = offsetTop + height;
  return {
    width,
    height,
    offsetTop,
    offsetBottom,
  };
};

export default getDimensions;
