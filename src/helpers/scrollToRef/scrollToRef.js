import getDimensions from './getDimensions';

const scrollToRef = (ref, offset = 0) => {
  // ele.current.scrollIntoView({
  //   behavior: 'smooth',
  //   block: 'start',
  // });
  const { offsetTop } = getDimensions(ref.current);
  window.scrollTo({
    top: offsetTop - offset,
    behavior: 'smooth',
  });
};

export default scrollToRef;
