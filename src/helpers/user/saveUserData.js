export default function saveUserData(_data) {
  const { token, ...data } = _data;
  localStorage.setItem('user_details', JSON.stringify({ ...data }));
  localStorage.setItem('token', token);
}
