import breadcrumbConfig from '@constants/breadcrumbsConfig';

const getBreadcrumbs = (_path = '') => {
  let title = breadcrumbConfig;
  let href = '';
  const breadcrumbs = [];
  const keys = _path.split('/');
  const totalKeys = keys.length;
  for (let index = 1; index < totalKeys - 1; index++) {
    if (keys[index]) {
      href += `/${keys[index]}`;
      title = title[keys[index]];
      if (title.index) breadcrumbs.push({ href, title: title.index });
    }
  }
  title = title[keys[totalKeys - 1] || 'index'] || 'Page not found';
  if (typeof title !== 'string') {
    title = title.index;
  }

  return { title, breadcrumbs };
};

export default getBreadcrumbs;
