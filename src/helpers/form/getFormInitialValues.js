export default function getFormInitialValues(_formConfig, _initialValues) {
  if (!Array.isArray(_formConfig) || !_formConfig.length) return {};
  const initialValues = _formConfig.reduce(
    (acc, { key, path, defaultValue }) => {
      if (!key) return acc;
      if (Array.isArray(path) && path.length && _initialValues) {
        let data;
        path.forEach((_path, idx) => {
          if (idx === 0) data = _initialValues?.[_path];
          else data = data?.[_path];
        });
        acc[key] = data?.[key] || defaultValue;
      } else acc[key] = _initialValues?.[key] || defaultValue;

      return acc;
    },
    {}
  );
  return initialValues;
}
