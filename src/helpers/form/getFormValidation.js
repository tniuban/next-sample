const getFormValidation = (_formConfig) => {
  if (!Array.isArray(_formConfig) || !_formConfig.length) return {};
  const validation = _formConfig.reduce((acc, { key, validation }) => {
    acc[key] = validation;
    return acc;
  }, {});
  return validation;
};

export default getFormValidation;
