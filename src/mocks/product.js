export const productMocks = [
  {
    id: 1,
    name: 'Iphone 13',
    price: 24990000,
    discountPrice: 20190000,
    thumbnail:
      'https://cdn.tgdd.vn/Products/Images/42/223602/TimerThumb/iphone-13-(4).jpg',
    rate: 4.1,
    totalComment: 221,
    giftPrice: 150000,
    details: [
      'Chip Apple A15 Bionic',
      'RAM 4 GB, ROM 128 GB',
      'Camera sau: 2 camera 12 MP',
      'Camera trước: 12 MP',
      'Pin 3240 mAh, Sạc 20 W',
    ],
  },
  {
    id: 2,
    name: 'Iphone 13 Pro',
    price: 30990000,
    discountPrice: 26390000,
    thumbnail:
      'https://cdn.tgdd.vn/Products/Images/42/230521/iphone-13-pro-sierra-blue-600x600.jpg',
    rate: 4.6,
    totalComment: 80,
    details: [
      'Chip Apple A15 Bionic',
      'RAM 6 GB, ROM 128 GB',
      'Camera sau: 3 camera 12 MP',
      'Camera trước: 12 MP',
      'Pin 3095 mAh, Sạc 20 W',
    ],
  },
  {
    id: 3,
    name: 'iPhone 13 Pro Max',
    price: 33990000,
    discountPrice: 28190000,
    thumbnail:
      'https://cdn.tgdd.vn/Products/Images/42/230529/TimerThumb/iphone-13-pro-max-(8).jpg',
    rate: 4.8,
    totalComment: 971,
    details: [
      'Chip Apple A15 Bionic',
      'RAM 6 GB, ROM 128 GB',
      'Camera sau: 3 camera 12 MP',
      'Camera trước: 12 MP',
      'Pin 4352 mAh, Sạc 20 W',
    ],
  },
  {
    id: 4,
    name: 'iPhone 13 Mini',
    price: 21990000,
    discountPrice: 18490000,
    thumbnail:
      'https://cdn.tgdd.vn/Products/Images/42/223602/TimerThumb/iphone-13-(4).jpg',
    rate: 4.2,
    totalComment: 768,
    details: [
      'Chip Apple A15 Bionic',
      'RAM 4 GB, ROM 128 GB',
      'Camera sau: 2 camera 12 MP',
      'Camera trước: 12 MP',
      'Pin 2438 mAh, Sạc 20 W',
    ],
  },
];
