export const caretoryMocks = [
  {
    id: '1232112465',
    name: 'Smartphone',
    code: 'smartphone',
    thumbnail: 'https://cdn.tgdd.vn//content/icon-phone-96x96-2.png',
  },
  {
    id: '123211223465',
    name: 'Laptop',
    code: 'laptop',
    thumbnail: 'https://cdn.tgdd.vn//content/icon-laptop-96x96-1.png',
  },
  {
    id: '123211231223465',
    name: 'Tablet',
    code: 'tablet',
    thumbnail: 'https://cdn.tgdd.vn//content/icon-tablet-96x96-1.png',
  },
];
