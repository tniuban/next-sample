import { api, SugarError } from '@services';

const findCategories = async ({ limit, offset }) => {
  const req = { limit, offset };
  const { data: dt = {} } = await api.post(
    '/category/find-categories',
    req
  );
  const { data, code, message, debugMessage } = dt
  if (code !== 0) {
    throw new SugarError(code, message, debugMessage);
  }
  return data;
};

export default findCategories;
