import { api, SugarError } from '@services';

const createCategory = async ({ name }) => {
  const req = { name };
  const { data: { data, code, message, debugMessage } = {} } = await api.post(
    '/category/create-category',
    req
  );
  if (code !== 0) {
    throw new SugarError(code, message, debugMessage);
  }
  return data;
};

export default createCategory;
