import { api, SugarError } from '@services';

const updateUser = async ({
  id,
  username,
  password,
  firstName,
  lastName,
  maidenName,
  gender,
  email,
  phone,
  birthDate,
  age,
  adress,
}) => {
  const req = {
    id,
    username,
    password,
    firstName,
    lastName,
    maidenName,
    gender,
    email,
    phone,
    birthDate,
    age,
    adress,
  };
  const { data: { data, code, message, debugMessage } = {} } = await api.post(
    '/user/update-user',
    req
  );
  if (code !== 0) {
    throw new SugarError(code, message, debugMessage);
  }
  return data;
};

export default updateUser;
