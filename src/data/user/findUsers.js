import { api, SugarError } from '@services';

const findUsers = async ({ limit, offset }) => {
  const req = { limit, offset };
  const { data: { data, code, message, debugMessage } = {} } = await api.post(
    '/user/find-users',
    req
  );
  if (code !== 0) {
    throw new SugarError(code, message, debugMessage);
  }
  return data;
};

export default findUsers;
