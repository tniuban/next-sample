import { api, SugarError } from '@services';

const deleteUser = async ({ id }) => {
  const req = { id };
  const { data: { data, code, message, debugMessage } = {} } = await api.post(
    '/user/delete-user',
    req
  );
  if (code !== 0) {
    throw new SugarError(code, message, debugMessage);
  }
  return data;
};

export default deleteUser;
