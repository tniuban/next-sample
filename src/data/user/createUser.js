import { api, SugarError } from '@services';

const createUser = async ({
  username,
  password,
  firstName,
  lastName,
  maidenName,
  gender,
  email,
  phone,
  birthDate,
  age,
  adress,
}) => {
  const req = {
    username,
    password,
    firstName,
    lastName,
    maidenName,
    gender,
    email,
    phone,
    birthDate,
    age,
    adress,
  };
  const { data: { data, code, message, debugMessage } = {} } = await api.post(
    '/user/create-user',
    req
  );
  if (code !== 0) {
    throw new SugarError(code, message, debugMessage);
  }
  return data;
};

export default createUser;
