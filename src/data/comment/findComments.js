import { api, SugarError } from '@services';

const findComments = async ({ productId, limit, offset }) => {
  const req = { productId, limit, offset };
  const { data: { data, code, message, debugMessage } = {} } = await api.post(
    '/comment/find-comments',
    req
  );
  if (code !== 0) {
    throw new SugarError(code, message, debugMessage);
  }
  return data;
};

export default findComments;
