import { api, SugarError } from '@services';

const createComment = async ({ productId, name, description, rate }) => {
  const req = {  productId, name, description, rate  };
  const { data: { data, code, message, debugMessage } = {} } = await api.post(
    '/comment/create-comment',
    req
  );
  if (code !== 0) {
    throw new SugarError(code, message, debugMessage);
  }
  return data;
};

export default createComment;
