import { api, SugarError } from '@services';

const login = async ({ username, password }) => {
  const req = { username, password };
  const { data: { data, code, message, debugMessage } = {} } = await api.post(
    '/auth/login',
    req
  );
  if (code !== 0) {
    throw new SugarError(code, message, debugMessage);
  }
  return data;
};

export default login;
