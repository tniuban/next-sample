import { api, SugarError } from '@services';

const login = async ({ refreshToken }) => {
  const req = { refreshToken };
  const { data: { data, code, message, debugMessage } = {} } = await api.post(
    '/auth/refresh-token',
    req
  );
  if (code !== 0) {
    throw new SugarError(code, message, debugMessage);
  }
  return data;
};

export default login;
