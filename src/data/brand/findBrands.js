import { api, SugarError } from '@services';

const findBrands = async ({ categoryIds, search, limit, offset }) => {
  const req = { categoryIds, search, limit, offset };
  const { data: { data, code, message, debugMessage } = {} } = await api.post(
    '/brand/find-brands',
    req
  );
  if (code !== 0) {
    throw new SugarError(code, message, debugMessage);
  }
  return data;
};

export default findBrands;
