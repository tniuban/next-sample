import { api, SugarError } from '@services';

const createBrand = async ({
  name,
  brand,
  stock,
  price,
  discountPrice,
  thumbnail,
  rate,
  totalComment,
  giftPrice,
  details,
  category,
}) => {
  const req = {
    name,
    brand,
    stock,
    price,
    discountPrice,
    thumbnail,
    rate,
    totalComment,
    giftPrice,
    details,
    category,
  };
  const { data: { data, code, message, debugMessage } = {} } = await api.post(
    '/product/create-product',
    req
  );
  if (code !== 0) {
    throw new SugarError(code, message, debugMessage);
  }
  return data;
};

export default createBrand;
