import { api, SugarError } from '@services';

const createProduct = async ({
  name,
  brandId,
  stock,
  price,
  discountPrice,
  thumbnail,
  rate,
  totalComment,
  giftPrice,
  details,
  categoryId,
}) => {
  const req = {
    name,
    brandId,
    stock,
    price,
    discountPrice,
    thumbnail,
    rate,
    totalComment,
    giftPrice,
    details,
    categoryId,
  };
  const { data: { data, code, message, debugMessage } = {} } = await api.post(
    '/product/create-product',
    req
  );
  if (code !== 0) {
    throw new SugarError(code, message, debugMessage);
  }
  return data;
};

export default createProduct;
