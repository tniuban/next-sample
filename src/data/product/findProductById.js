import { api, SugarError } from '@services';

const findProductById = async ({ id }) => {
  const req = { id };
  const { data: { data, code, message, debugMessage } = {} } = await api.post(
    '/product/find-product-by-id',
    req
  );
  if (code !== 0) {
    throw new SugarError(code, message, debugMessage);
  }
  return data;
};

export default findProductById;
