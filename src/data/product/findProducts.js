import { api, SugarError } from '@services';

const findProducts = async ({
  categoryId,
  brandId,
  search,
  limit,
  offset,
}) => {
  const req = { categoryId, brandId, search, limit, offset };
  const { data: { data, code, message, debugMessage } = {} } = await api.post(
    '/product/find-products',
    req
  );
  if (code !== 0) {
    throw new SugarError(code, message, debugMessage);
  }
  return data;
};

export default findProducts;
