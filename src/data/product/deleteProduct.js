import { api, SugarError } from '@services';

const deleteProduct = async ({ id }) => {
  const req = { id };
  const { data: { data, code, message, debugMessage } = {} } = await api.post(
    '/product/delete-product',
    req
  );
  if (code !== 0) {
    throw new SugarError(code, message, debugMessage);
  }
  return data;
};

export default deleteProduct;
