import { api, SugarError } from '@services';

const updateProduct = async ({
  id,
  name,
  brand,
  stock,
  price,
  discountPrice,
  thumbnail,
  rate,
  totalComment,
  giftPrice,
  details,
  category,
}) => {
  const req = {
    id,
    name,
    brand,
    stock,
    price,
    discountPrice,
    thumbnail,
    rate,
    totalComment,
    giftPrice,
    details,
    category,
  };
  const { data: { data, code, message, debugMessage } = {} } = await api.post(
    '/product/update-product',
    req
  );
  if (code !== 0) {
    throw new SugarError(code, message, debugMessage);
  }
  return data;
};

export default updateProduct;
