import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import { useDispatch, useSelector } from 'react-redux';
import styles from './Toast.module.scss';
import clsx from 'clsx';
import { deleteToast } from '@redux/actions/toast';

const timeoutDisappear = 700;
const Toast = ({ timeout = 3000 }) => {
  const toasts = useSelector((state) => state.toast);
  const [willRemoveId, setWillRemoveId] = useState();
  const dispatch = useDispatch();
  useEffect(() => {
    const interval = setInterval(() => {
      if (toasts.length && !willRemoveId) {
        setWillRemoveId(toasts[0].id);
      }
    }, timeout - timeoutDisappear);

    return () => {
      clearInterval(interval);
    };
  }, [willRemoveId, toasts, timeout, dispatch]);

  useEffect(() => {
    const interval = setInterval(() => {
      if (willRemoveId) {
        dispatch(deleteToast(willRemoveId));
        setWillRemoveId(null);
      }
    }, timeoutDisappear);

    return () => {
      clearInterval(interval);
    };
  }, [willRemoveId, timeout, dispatch]);

  if (!toasts?.length) return null;

  return ReactDOM.createPortal(
    <div className={clsx([styles.container, styles.position])}>
      {toasts.map(({ id, title, description, backgroundColor }) => (
        <div
          data-testid="toast-container"
          key={id}
          className={clsx([
            styles.notification,
            styles.toast,
            { [styles.disappear]: willRemoveId === id },
          ])}
          style={{ backgroundColor: backgroundColor }}
        >
          <button
            data-testid="close-toast-button"
            onClick={() => setWillRemoveId(id)}
          >
            <i className="fa-solid fa-circle-xmark" />
          </button>
          <div className={styles.notificationBox}>
            <p data-testid="toast-title" className={styles.title}>
              {title}
            </p>
            <p data-testid="toast-description" className={styles.description}>
              {description}
            </p>
          </div>
        </div>
      ))}
    </div>,
    document.querySelector('body')
    // document.getElementById('toast-backdrop')
  );
};

export default Toast;
