import React from 'react';
import { poster } from '@services';
import Divider from '@components/Divider';
import { useDispatch } from 'react-redux';
import { addToast } from '@redux/actions/toast';
import {
  getFormInitialValues,
  getFormValidation,
  useValidate,
} from '@helpers/form';
import { toastType } from '@constants/toast';
import { formConfig } from '@constants/productFormConfig';

const UserForm = ({ initialValues }) => {
  const dispatch = useDispatch();
  const {
    values,
    errors,
    resetForm,
    handleSubmit,
    setFieldError,
    handleChange,
    setSubmitting,
    isSubmitting,
  } = useValidate({
    initialValues: getFormInitialValues(formConfig, initialValues),
    handleValidation: getFormValidation(formConfig),
    validateOnChange: true,
    onSubmit: async (val) => {
      const isNewUser = !val.id;
      const body = {
        id: val.id,
        name: val.name,
        brandId: val.brandId,
        stock: val.stock,
        price: val.price,
        discountPrice: val.discountPrice,
        thumbnail: val.thumbnail,
        rate: val.rate,
        totalComment: val.totalComment,
        giftPrice: val.giftPrice,
        details: val.details.split("'\n"),
        categoryId: val.categoryId,
      };
      const { data: resp, error } = await poster(
        isNewUser
          ? '/api/product/create-product'
          : '/api/product/update-product',
        body
      );
      if (resp?.status && !error) {
        dispatch(
          addToast({
            type: toastType.SUCCESS,
            title: isNewUser
              ? 'Created product sucessfully'
              : 'Updated product sucessfully',
            description: '',
          })
        );
        resetForm();
      } else {
        dispatch(
          addToast({
            type: toastType.ERROR,
            title: isNewUser
              ? 'Faild to create product'
              : 'Faild to update product',
            description: resp?.message || error?.message,
          })
        );
      }
      setSubmitting(false);
    },
  });

  const handleOnChangeInput = (event) => {
    const { name, value, type } = event.target;
    let val;
    if (type === 'number') {
      val = Number(value);
    } else val = value;
    setFieldError(name, undefined);
    handleChange({ target: { name, value: val } });
  };

  const isNewUser = !Boolean(values?.id);

  return (
    <div className='container-sm' style={{ maxWidth: 800 }}>
      <h2 className='text-uppercase fw-bold text-center py-4'>
        {isNewUser ? 'create a new product' : 'update product'}
      </h2>
      <div className='alert alert-info' role='alert'>
        Fill the form below to{' '}
        {isNewUser ? 'create a new product' : 'update product'}
      </div>
      <div>
        <Divider>Account Details</Divider>
        <div className='row'>
          {formConfig.map(
            ({
              key,
              hidden,
              disabled,
              Component,
              query,
              className = 'col-12 col-sm-6',
              ...others
            }) => {
              if (hidden) return null;
              return (
                <div key={key || uuid()} className={className}>
                  <Component
                    disabled={disabled || isSubmitting}
                    className={className}
                    name={key}
                    id={key}
                    onChange={handleOnChangeInput}
                    value={values[key]}
                    error={errors[key]}
                    {...(query && {
                      query: { categoryIds: [values.categoryId] },
                    })}
                    {...others}
                  />
                </div>
              );
            }
          )}
        </div>
        <div className='d-grid gap-2 col-10 mx-auto'>
          <button
            data-testid='form-button-submit'
            onClick={handleSubmit}
            disabled={isSubmitting}
            type='submit'
            className='btn btn-primary my-3'
          >
            Submit
          </button>
        </div>
      </div>
    </div>
  );
};

export default UserForm;
