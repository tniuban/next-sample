import React from 'react';
import ProductCard from './Card';
import { useTable } from '@helpers/table';
import Loading from '@components/Loading';
import Error from '@components/Errror';

export default function ProductList() {
  const { data, loading, error } = useTable({
    limit: -1,
    skip: 0,
    keyData: 'products',
    api: '/api/product/find-products',
    query: { categoryId },
  });
  if (loading) {
    return <Loading />;
  }
  if (error) {
    return <Error message={error} />;
  }
  return (
    <div className='row mx-auto container'>
      {data.map(
        (
          {
            id,
            thumbnail,
            name,
            discountPrice,
            price,
            rate,
            totalComment,
            giftPrice,
            details,
          },
          idx
        ) => (
          <Link href={`/product/${id}`} key={id + '_' + idx}>
            <div className="px-1 pt-2 px-md-2 pt-md-3 col-6 col-sm-6 col-lg-3 col-xxl-2">
              <ProductCard
                id={id}
                thumbnail={thumbnail}
                name={name}
                discountPrice={discountPrice}
                price={price}
                rate={rate}
                totalComment={totalComment}
                giftPrice={giftPrice}
                details={details}
              />
            </div>
          </Link>
        )
      )}
    </div>
  );
}
