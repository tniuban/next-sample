/* eslint-disable @next/next/no-img-element */
import React from 'react';
import {
  formatCurrency,
  formatNumberWithSeparator,
} from '@helpers/formatNumber';
import styles from './Card.module.scss';
import { useRouter } from 'next/router';

export default function ProductCard({
  id,
  thumbnail,
  name,
  discountPrice,
  price,
  rate,
  totalComment,
  giftPrice,
  details,
}) {
  const router = useRouter();
  const discountPercent = 100 * (1 - discountPrice / price);
  return (
    <div
      className={styles.card_container}
      onClick={() => router.push(`/product/${id}`)}
    >
      <div className={styles.item_img}>
        <img src={thumbnail} alt={name} className={styles.img} />
      </div>

      <p className={styles.item_name}>{name}</p>

      <strong className={styles.item_price}>
        {formatCurrency(discountPrice || price)}
        {discountPercent && (
          <small className={styles.item_discount_percent}>
            -{formatNumberWithSeparator(discountPercent, 0, 0)}%
          </small>
        )}
      </strong>

      {Boolean(giftPrice) && (
        <span className={styles.item_gift}>
          Gift {formatCurrency(giftPrice)}
        </span>
      )}

      <div className={styles.item_vote}>
        <b>
          {formatNumberWithSeparator(rate)}&nbsp;
          <span className='fa-solid fa-star' />
        </b>
        &nbsp;
        {/* {totalComment && <span>({to})</span>} */}
        {totalComment ? `(${totalComment})` : ''}
      </div>

      {Array.isArray(details) && (
        <ul className={styles.item_details}>
          {details.map((detail) => (
            <li key={detail}>{detail}</li>
          ))}
        </ul>
      )}
    </div>
  );
}
