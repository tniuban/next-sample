/* eslint-disable @next/next/no-img-element */
import Error from '@components/Errror';
import Loading from '@components/Loading';
import RatingStar from '@components/RatingStar';
import {
  formatCurrency,
  formatNumberWithSeparator,
} from '@helpers/formatNumber';
import { scrollToRef } from '@helpers/scrollToRef';
import { addProductToCart } from '@redux/actions/cart';
import { poster } from '@services';
import Router from 'next/router';
import React from 'react';
import { useDispatch } from 'react-redux';
import useSWR from 'swr';
import styles from './ProductTechDetails.module.scss';

export default function ProductDetails({ id, commentRef }) {
  const { data: { data: resp } = {}, error } = useSWR(
    ['/api/product/find-product-by-id', id],
    (_url, _id) => poster(_url, { id: _id })
  );
  const dispatch = useDispatch();
  const addToCard = () => {
    dispatch(
      addProductToCart({
        id: data.id,
        thumbnail: data.thumbnail,
        name: data.name,
        price: data.price,
        discountPrice: data.discountPrice,
      })
    );
  };
  if (!error && !resp) {
    return <Loading />;
  }
  const { data, message } = resp;
  if (error || message) {
    return <Error message={error || message} />;
  }
  return (
    <div className='d-flex flex-column flex-md-row align-items-center pb-4'>
      <div>
        <img
          className={styles.thumbnail}
          src={data.thumbnail}
          alt={data.name}
        />
      </div>
      <div className='w-100'>
        <div className={styles.name}>
          <h3>{data.name}</h3>
        </div>
        {/* TECHNICAL DETAIL */}
        <div className='d-flex align-items-center justify-content-between'>
          <span className={styles.brand}>
            Brand: <strong>{data.brandName}</strong>
          </span>
          <div
            className='d-flex align-items-center'
            onClick={() => scrollToRef(commentRef)}
          >
            <RatingStar value={data.rate} />
            {data.totalComment && (
              <span className={styles.totalComment}>
                ({data.totalComment} review{data.totalComment > 1 ? 's' : ''})
              </span>
            )}
          </div>
        </div>
        {/* PRICE DETAIL */}
        <div className={styles.price_container}>
          {data.discountPrice && (
            <span className={styles.discount_percent}>
              {formatNumberWithSeparator(
                ((data.discountPrice - data.price) * 100) / data.price,
                0,
                0
              )}
              %
            </span>
          )}
          <span className={styles.discount_price}>
            {formatCurrency(data.discountPrice || data.price)}
          </span>
          {data.discountPrice && (
            <strike className={styles.price}>
              {formatCurrency(data.price)}
            </strike>
          )}
        </div>
        {/* TECHNICAL DETAIL */}
        <ul className={styles.detail_container}>
          {data.details.map((detail) => (
            <li className={styles.detail} key={detail}>
              {detail}
            </li>
          ))}
        </ul>
        <div className='d-flex flex-column flex-md-row align-items-center'>
          <button
            type='button'
            onClick={() => {
              addToCard();
              Router.push('/cart');
            }}
            className='btn btn-danger w-100 py-2 px-4 m-2'
          >
            Buy now
          </button>
          <button
            type='button'
            onClick={addToCard}
            className='btn btn-primary w-100 py-2 px-4 m-2'
          >
            <i className='fa-solid fa-cart-plus mr-2' />
            &nbsp;Add to cart
          </button>
        </div>
      </div>
    </div>
  );
}
