import React from 'react';
import RatingStar from '@components/RatingStar';
import moment from 'moment';
import styles from './CommentItem.module.scss';

export default function CommentItem({
  createdBy,
  createdAt,
  rate,
  description,
}) {
  return (
    <div className={styles.container}>
      <div className={styles.created_by_container}>
        <div>
          <span className={styles.acronym_name}>{createdBy.firstName[0]}</span>
          <span className={styles.created_by}>
            {createdBy.firstName} {createdBy.lastName}
          </span>
        </div>
        <span className={styles.created_at}>
          {moment(createdAt).format('hh:mmA, DD/MM/YY')}
        </span>
      </div>
      <div className={styles.review_container}>
        <div className={styles.rating}>
          <b>Rate:</b>&nbsp;
          <RatingStar value={rate} />
        </div>
        <p className={styles.description}>
          <b>Review:</b>&nbsp;{description}
        </p>
      </div>
    </div>
  );
}
