import React from 'react';
import Error from '@components/Errror';
import Loading from '@components/Loading';
import CommentItem from './CommentItem';
import useSWR from 'swr';
import { poster } from '@services';
import CommentForm from './CommentForm';

export default function Comments({ productId }) {
  const { data: { data: resp } = {}, error } = useSWR(
    ['/api/comment/find-comments', productId],
    (_url, _productId) => poster(_url, { productId: _productId })
  );
  if (!error && !resp) {
    return <Loading />;
  }
  const { data, message } = resp || {};
  if (error || message) {
    return <Error message={error || message} />;
  }
  return (
    <div
      style={{
        boxShadow:
          '0 1px 2px 0 rgb(60 64 67 / 10%), 0 2px 6px 2px rgb(60 64 67 / 15%)',
        borderRadius: 12,
        padding: '1rem 1rem 0',
      }}
    >
      {data.comments?.map((element) => (
        <CommentItem key={element.id} {...element} />
      ))}
      <CommentForm productId={productId} />
    </div>
  );
}
