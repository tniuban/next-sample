import React from 'react';
import { useValidate, validateHelpers } from '@helpers/form';
import styles from './CommentForm.module.scss';
import TextArea from '@components/TextArea';
import RatingStar from '@components/RatingStar';
import { poster } from '@services';

export default function CommentForm({ productId }) {
  const {
    values,
    errors,
    handleChange,
    handleSubmit,
    setFieldValue,
    setFieldError,
  } = useValidate({
    initialValues: {
      name: '',
      description: '',
      rate: 0,
    },
    onSubmit: async (val) => {
      const body = {
        productId,
        name: val.name,
        description: val.description,
        rate: val.rate,
      };
      await poster('/api/comment/create-comment', body);
    },
    validateOnChange: true,
    handleValidation: {
      name: [validateHelpers.string(), validateHelpers.required()],
      description: [validateHelpers.string(), validateHelpers.required()],
      rate: [validateHelpers.number(), validateHelpers.required()],
    },
  });
  const onChange = (event) => {
    handleChange(event);
    setFieldError(event.target.name, null);
  };
  return (
    <div className={styles.container}>
      <div className="d-flex align-items-center">
        <b>Write your review:</b>
        <div>
          <RatingStar
            value={values.rate}
            onChange={(value) => {
              setFieldValue('rate', value);
              setFieldError('rate', null);
            }}
          />
        </div>
      </div>
      <TextArea
        disabledLabel
        name="description"
        value={values.description}
        error={errors.description}
        onChange={onChange}
        placeholder="Write your description"
        disabledError
      />
      <button
        onClick={handleSubmit}
        type="button"
        className="btn btn-primary w-100 my-3"
      >
        Push review
      </button>
    </div>
  );
}
