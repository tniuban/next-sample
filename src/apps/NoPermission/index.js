/* eslint-disable @next/next/no-img-element */
import Link from 'next/link';
import React from 'react';
import styles from './NoPermission.module.scss';

export default function NoPermission() {
  return (
    <div className={styles.container}>
      <div className={styles.forbidden_container}>
        <div className={styles.forbidden_header}>
          <div className={styles.status_container}>
            <p className={styles.status}>403</p>
            <p className={styles.status_name}>Access denied</p>
          </div>
          <div className={styles.img_container}>
            <img src='/img/403.jpg' alt='a' />
          </div>
        </div>
        <div className={styles.forbidden_text}>
          We are sorry, but you do not have access to this page or resourse.
        </div>
        <Link href='/'>
          <div className={styles.forbidden_navigator}>Back to homepage</div>
        </Link>
      </div>
    </div>
  );
}
