import Link from 'next/link';
import React from 'react';
import styles from './NoProduct.module.scss';

export default function NoProduct() {
  return (
    <div className='container-sm mt-5' style={{ maxWidth: 800 }}>
      <div className={styles.cart_empty} />
      <p className='text-center'>There are no products in the cart</p>
      <Link href='/product'>
        <button className='w-100 d-block btn btn-outline-primary mx-auto'>
          Go to home
        </button>
      </Link>
      <p className='text-center mt-3 '>
        When you need help, please call&nbsp;
        <a className='text-primary' href='tel:18001060'>
          1800.1060
        </a>
        &nbsp;or&nbsp;
        <a className='text-primary' href='tel:02836221060'>
          028.3622.1060
        </a>
        &nbsp;(7h30 - 22h)
      </p>
    </div>
  );
}
