import { formatCurrency } from '@helpers/formatNumber';
import React from 'react';
import styles from './ProvisionalTotal.module.scss';

export default function Total({ totalPrice, totalQuantity }) {
  return (
    <div className={styles.container}>
      <p>Provisional total ({totalQuantity} products):</p>
      <p className={styles.price}>{formatCurrency(totalPrice)}</p>
    </div>
  );
}
