/* eslint-disable @next/next/no-img-element */
import {
  formatCurrency,
  formatNumberWithSeparator,
} from '@helpers/formatNumber';
import {
  changeProductQuantityInCart,
  removeProductFromCart,
} from '@redux/actions/cart';
import React from 'react';
import { useDispatch } from 'react-redux';
import styles from './CartItem.module.scss';

const quantityConfig = {
  max: 100099990,
  min: 1,
};

export default function CartItem({
  id,
  name,
  price,
  discountPrice,
  thumbnail,
  color = 'Red',
  quantity,
  //   giftPrice = 100000,
}) {
  const dispatch = useDispatch();
  const handleRemoveProduct = (_id) => () => {
    dispatch(removeProductFromCart(_id));
  };

  const handleChangeQuantity = (event) => {
    event.preventDefault();
    const { value } = event.target;
    const valueToNumber = Number(value.replace(/,/g, ''));
    if (isNaN(valueToNumber)) return;
    const _quantity =
      (valueToNumber < quantityConfig.min && quantityConfig.min) ||
      (valueToNumber > quantityConfig.max && quantityConfig.max) ||
      valueToNumber;
    dispatch(
      changeProductQuantityInCart({
        id,
        quantity: _quantity,
      })
    );
  };

  const handleClick = (_offset) => () => {
    dispatch(
      changeProductQuantityInCart({
        id,
        quantity: quantity + _offset,
      })
    );
  };
  return (
    <div className={styles.card_item_container}>
      <div className={styles.img_container}>
        <img src={thumbnail} alt={name} className={styles.img} />
        <div className={styles.remove} onClick={handleRemoveProduct(id)}>
          <i className='fa-solid fa-xmark' />
          &nbsp;Remove
        </div>
      </div>
      <div className={styles.details_container}>
        <p className={styles.name}>{name}</p>
        {/* {giftPrice && (
          <p className={styles.item_gift}>Gift {formatCurrency(giftPrice)}</p>
        )} */}
        <p className={styles.gift}>
          Nhập mã MWG18 giảm 3% tối đa 100.000đ khi thanh toán quét QRcode qua
          App của ngân hàng
        </p>
        <p className={styles.color}>Color: {color}</p>
      </div>
      <div className={styles.price_quantity_container}>
        <div className={styles.price_container}>
          <p className={styles.discount_price}>
            {formatCurrency(discountPrice)}
          </p>
          <strike className={styles.price}>{formatCurrency(price)}</strike>
        </div>
        <div className={styles.quantity_container}>
          <button className={styles.quantity_button} onClick={handleClick(-1)}>
            -
          </button>
          <input
            onChange={handleChangeQuantity}
            value={formatNumberWithSeparator(quantity)}
            inputMode='numeric'
            className={styles.quantity_input}
          />
          <button className={styles.quantity_button} onClick={handleClick(+1)}>
            +
          </button>
        </div>
      </div>
    </div>
  );
}
