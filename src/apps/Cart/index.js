import React from 'react';
import { useSelector } from 'react-redux';
import CartItem from './CartItem';
import NoProduct from './NoProduct';
import ProvisionalTotal from './ProvisionalTotal';

export default function Card() {
  const cart = useSelector((state) => state.cart);
  const { items, totalQuantity, totalPrice } = cart;
  if (!totalQuantity) return <NoProduct />;
  return (
    <div className='container-sm mt-5' style={{ maxWidth: 800 }}>
      {items.map(({ id, name, price, discountPrice, thumbnail, quantity }) => (
        <CartItem
          key={id}
          id={id}
          name={name}
          price={price}
          discountPrice={discountPrice}
          thumbnail={thumbnail}
          quantity={quantity}
        />
      ))}
      <ProvisionalTotal totalQuantity={totalQuantity} totalPrice={totalPrice} />
    </div>
  );
}
