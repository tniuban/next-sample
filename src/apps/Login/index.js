import React from 'react';
import {
  getFormInitialValues,
  getFormValidation,
  useValidate,
} from '@helpers/form';
import loginConfig from '@constants/loginConfig';
import { v4 as uuidv4 } from 'uuid';
import { saveUserData } from '@helpers/user';
import { useDispatch } from 'react-redux';
import { addToast } from '@redux/actions/toast';
import { toastType } from '@constants/toast';
import CheckBox from '@components/CheckBox';
import styles from './Login.module.scss';
import Link from 'next/link';
import { poster } from '@services';
import Router from 'next/router';

export default function Login() {
  const dispatch = useDispatch();
  const { values, errors, handleSubmit, handleChange, setFieldError } =
    useValidate({
      initialValues: getFormInitialValues(loginConfig),
      validateOnChange: false,
      handleValidation: getFormValidation(loginConfig),
      onSubmit: async (value) => {
        const body = {
          username: value.username,
          password: value.password,
        };
        const { data } = await poster('/api/auth/login', body);
        if (data.status) {
          dispatch(
            addToast({
              type: toastType.SUCCESS,
              title: 'Login sucessfully!',
              description: '',
            })
          );
          Router.reload();
          return;
        }
        dispatch(
          addToast({
            type: toastType.ERROR,
            title: 'Failed to login',
            description: data.message,
          })
        );
        // throw Error(error);
      },
    });

  const onChange = (event) => {
    event.preventDefault();
    handleChange(event);
    setFieldError(event.target.name, undefined);
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className={styles.header}>
        <span className="fs-3">Sign in</span>
      </div>
      <div className="pt-5 pb-3">
        {loginConfig.map(({ hidden, Component, key, ...others }) => {
          if (hidden) return null;
          return (
            <div key={key || uuidv4}>
              <Component
                disabledLabel
                name={key}
                value={values[key]}
                error={errors[key]}
                onChange={onChange}
                {...others}
              />
            </div>
          );
        })}
      </div>
      <div className="d-flex align-items-center justify-content-between pb-3">
        <Link href="/reset-password">
          <a className={styles.link}>Forgot password?</a>
        </Link>
        <CheckBox context="Remember me" />
      </div>
      <button type="submit" className="btn btn-primary w-100">
        Submit
      </button>

      <p className="text-center mt-5">
        Do you have an account?&nbsp;
        <Link href="/reset-password">
          <a className={styles.link}>Create an account</a>
        </Link>
      </p>
    </form>
  );
}
