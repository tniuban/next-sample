import Link from 'next/link';
import React from 'react';
import styles from './404.module.scss';

export default function NotFound() {
  return (
    <div className={styles.notfound_container}>
      <div className={styles.notfound}>
        <div className={styles.notfound_404}>
          <h1>
            4<span></span>4
          </h1>
        </div>
        <h2>Oops! Page Not Be Found</h2>
        <p>
          The page you are looking for might have been removed had its name
          changed or is temporarily unavailable
        </p>
        <Link href='/'>
          <button type='button' className='btn btn-outline-info'>
            Back to homepage
          </button>
        </Link>
      </div>
    </div>
  );
}
