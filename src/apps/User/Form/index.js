import React from 'react';
import moment from 'moment';
import { poster } from '@services';
import Divider from '@components/Divider';
import { useDispatch } from 'react-redux';
import { addToast } from '@redux/actions/toast';
import {
  getFormInitialValues,
  getFormValidation,
  useValidate,
} from '@helpers/form';
import { toastType } from '@constants/toast';
import { formConfig } from '@constants/userFormConfig';

const UserForm = ({ initialValues }) => {
  const dispatch = useDispatch();
  const {
    values,
    errors,
    resetForm,
    handleSubmit,
    setFieldError,
    handleChange,
    setSubmitting,
    isSubmitting,
  } = useValidate({
    initialValues: getFormInitialValues(formConfig, initialValues),
    handleValidation: getFormValidation(formConfig),
    validateOnChange: true,
    onSubmit: async (val) => {
      const isNewUser = !val.id;
      const body = {
        id: val.id,
        username: val.username,
        password: val.password,
        firstName: val.firstName,
        lastName: val.lastName,
        maidenName: val.maidenName,
        gender: val.gender,
        email: val.email,
        phone: val.phone,
        birthDate: moment(val.birthDate).format('yyyy-mm-dd'),
        address: {
          address: val.address,
          city: val.city,
          postalCode: val.postalCode,
          state: val.state,
        },
        age: val.age,
      };
      const { data: resp, error } = await poster(
        isNewUser ? '/api/user/create-user' : '/api/user/update-user',
        body
      );
      if (resp?.status && !error) {
        dispatch(
          addToast({
            type: toastType.SUCCESS,
            title: isNewUser
              ? 'Created user sucessfully'
              : 'Updated user sucessfully',
            description: '',
          })
        );
        resetForm();
      } else {
        dispatch(
          addToast({
            type: toastType.ERROR,
            title: isNewUser
              ? 'Faild to create user'
              : 'Faild to update user',
            description: resp?.message || error?.message,
          })
        );
      }
      setSubmitting(false);
    },
  });

  const handleOnChangeInput = (event) => {
    const { name, value, type } = event.target;
    let val;
    if (type === 'number') {
      val = Number(value);
    } else val = value;
    setFieldError(name, undefined);
    handleChange({ target: { name, value: val } });
  };

  const isNewUser = !Boolean(values?.id);

  return (
    <div className='container-sm' style={{ maxWidth: 800 }}>
      <h2 className='text-uppercase fw-bold text-center py-4'>
        {isNewUser ? 'create a new user' : 'update user'}
      </h2>
      <div className='alert alert-info' role='alert'>
        Fill the form below to {isNewUser ? 'create a new user' : 'update user'}
      </div>
      <div>
        <Divider>Account Details</Divider>
        <div className='row'>
          {formConfig.map(
            ({
              key,
              hidden,
              disabled,
              Component,
              className = 'col-12 col-sm-6',
              ...others
            }) => {
              if (hidden) return null;
              return (
                <div key={key || uuid()} className={className}>
                  <Component
                    disabled={disabled || isSubmitting}
                    className={className}
                    name={key}
                    id={key}
                    onChange={handleOnChangeInput}
                    value={values[key]}
                    error={errors[key]}
                    {...others}
                  />
                </div>
              );
            }
          )}
        </div>
        <div className='d-grid gap-2 col-10 mx-auto'>
          <button
            data-testid='form-button-submit'
            onClick={handleSubmit}
            disabled={isSubmitting}
            type='submit'
            className='btn btn-primary my-3'
          >
            Submit
          </button>
        </div>
      </div>
    </div>
  );
};

export default UserForm;
