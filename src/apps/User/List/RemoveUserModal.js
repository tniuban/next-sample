import React from 'react';
import Modal from '@components/Modal';

const RemoveUserModal = ({ open, onClose, handleRemove }) => (
  <Modal title='Delete this user' open={open} onClose={onClose}>
    <div className='modal-body'>
      This user will be deleted and is cannot be undone. Are you sure?
    </div>
    <div className='modal-footer'>
      <button
        data-testid='confirm-button-yes'
        type='button'
        className='btn btn-primary'
        onClick={handleRemove}
      >
        Remove
      </button>
      <button
        type='button'
        onClick={onClose}
        className='btn btn-outline-primary'
      >
        Cancel
      </button>
    </div>
  </Modal>
);

export default RemoveUserModal;
