/* eslint-disable @next/next/no-img-element */
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addToast } from '@redux/actions/toast';
import Link from 'next/link';
import {
  RowLimit,
  TableBody,
  TableHeader,
  TablePagination,
} from '@components/Table';
import { deleter, poster } from '@services';
import { toastType } from '@constants/toast';
import useTable from '@helpers/table/useTable';
import RemoveUserModal from './RemoveUserModal';

const noAvatarUrl =
  'https://tse2.mm.bing.net/th/id/OIP.1QE_bLwBgy4tLarLPJYrEAHaHa?pid=ImgDet&rs=1';

export default function UserList() {
  const [prepareToRemoveId, setPrepareToRemoveId] = useState(null);
  const dispatch = useDispatch();
  const {
    data,
    setData,
    limit,
    setLimit,
    skip,
    setSkip,
    loading,
    error,
    totalPage,
    currentPageIdx,
  } = useTable({
    limit: 10,
    skip: 0,
    keyData: 'users',
    api: '/api/user/find-users',
  });
  const handleRemoveUser = (_id) => async () => {
    if (!_id) return; // because id of user is a number greater than 0
    const { data: resp, error } = await poster('/api/user/delete-user', {
      id: _id,
    });
    if (resp?.status && !error) {
      const newCustomers = data.filter(({ id }) => id !== _id);
      setData(newCustomers);
      dispatch(
        addToast({
          type: toastType.SUCCESS,
          title: 'Removed user successfully!',
          description: '',
        })
      );
    } else {
      dispatch(
        addToast({
          type: toastType.ERROR,
          title: 'Faild to remove user',
          description: resp?.message || error?.message,
        })
      );
    }
    setPrepareToRemoveId(null);
  };

  const handlePagination = (_pageIndex) => () => {
    const skip = _pageIndex * limit;
    setSkip(skip);
  };

  const columns = [
    {
      id: 'index',
      label: 'No.',
      renderCell: (idx) => idx + 1 + skip,
      props: {
        className: 'text-center',
      },
    },
    {
      id: 'image',
      label: 'Avatar',
      props: { className: 'text-center' },
      renderCell: (val) => (
        <div className='d-flex justify-content-center'>
          <img
            src={val || noAvatarUrl}
            alt={val}
            width={50}
            height={50}
            style={{
              borderRadius: '50%',
              objectFit: 'cover',
              boxShadow: 'rgba(149, 157, 165, 0.2) 0px 8px 24px',
              border: '1px solid #ADADAD',
            }}
          />
        </div>
      ),
    },
    {
      label: 'Name',
      renderCell: (_, fullVal) => `${fullVal.firstName} ${fullVal.lastName}`,
    },
    {
      id: 'age',
      label: 'Age',
      props: {
        className: 'text-center',
      },
    },
    {
      id: 'gender',
      label: 'Gender',
      props: {
        style: {
          textAlign: 'center',
          textTransform: 'capitalize',
        },
      },
    },
    { id: 'email', label: 'Email' },
    {
      id: 'address',
      label: 'Address',
      renderCell: (val) => (
        <>
          {val.address}
          {val.city ? `, ${val.city}` : ''}
          {val.state ? `, ${val.state}` : ''}
          {val.postalCode ? `, ${val.postalCode}` : ''}
        </>
      ),
    },
    {
      id: 'id',
      label: 'Action',
      props: {
        className: 'text-center',
      },
      renderCell: (id) => (
        <div className='d-flex justify-content-around align-items-center'>
          <Link href={`/user/update/${id}`}>
            <button
              type='button'
              title='Update user'
              className='btn btn-outline-dark rounded-5 mx-2'
            >
              <i className='fa-solid fa-pen'></i>
            </button>
          </Link>
          <button
            type='button'
            title='Remove user'
            className='btn btn-outline-dark rounded-5'
            onClick={() => setPrepareToRemoveId(id)}
          >
            <i className='fa-solid fa-trash'></i>
          </button>
        </div>
      ),
    },
  ];

  return (
    <>
      <RemoveUserModal
        open={prepareToRemoveId}
        onClose={() => setPrepareToRemoveId(null)}
        handleRemove={handleRemoveUser(prepareToRemoveId)}
      />
      <div className='container-lg'>
        <table className='table table-hover align-middle'>
          <TableHeader title='User list Management' columns={columns} />
          <TableBody
            columns={columns}
            loading={loading}
            error={error}
            data={data}
          />

          <tfoot>
            <tr>
              <td colSpan={columns.length}>
                <div className='d-flex justify-content-between align-items-center'>
                  <TablePagination
                    currentPageIdx={currentPageIdx}
                    totalPage={totalPage}
                    loading={loading}
                    handlePagination={handlePagination}
                  />
                  <RowLimit
                    limitOptions={[5, 10, 20, 50]}
                    currentLimit={limit}
                    handleChangeLimit={setLimit}
                  />
                </div>
              </td>
            </tr>
          </tfoot>
        </table>
      </div>
    </>
  );
}
