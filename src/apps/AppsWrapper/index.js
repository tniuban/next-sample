import React from 'react';
import { useSelector } from 'react-redux';
import clsx from 'clsx';
import Sidebar from '@components/Sidebar';
import Header from '@components/Header';
import headerConfig from '@constants/headerConfig';
import styles from './AppsWrapper.module.scss';

export default function AppsWrapper({ children }) {
  const { isOpen } = useSelector((state) => state.sidebar);

  return (
    <div className={styles.wrapper}>
      <Sidebar isOpen={isOpen} />
      <div className={clsx([styles.content, { [styles.is_open]: isOpen }])}>
        <div className={styles.header}>
          <Header listItems={headerConfig} />
        </div>
        {children}
      </div>
    </div>
  );
}
