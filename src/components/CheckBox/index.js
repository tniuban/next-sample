import React from 'react';

export default function CheckBox({ value, context, onChange, id }) {
  return (
    <div className="form-check">
      <input
        className="form-check-input"
        type="checkbox"
        value={value}
        id={id}
        onChange={onChange}
      />
      {context && (
        <label className="form-check-label" htmlFor={id}>
          {context}
        </label>
      )}
    </div>
  );
}
