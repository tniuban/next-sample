import React from "react";

const Loading = () => (
  <div
    data-testid="loading-box"
    className="d-flex align-items-center justify-content-center py-4"
  >
    <div
      data-testid="loading-spin"
      className="spinner-border text-primary"
      role="status"
    />
    <span data-testid="loading-text" className="text-primary mx-2">
      Loading...
    </span>
  </div>
);

export default Loading;
