import React from 'react';

const Divider = ({ children }) => (
  <p data-testid='divider' className='text-uppercase text-primary fw-bold fs-5'>
    {children}
  </p>
);

export default Divider;
