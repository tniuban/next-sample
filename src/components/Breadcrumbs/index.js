import React from 'react';
import clsx from 'clsx';
import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { getBreadcrumbs } from '@helpers/breadcrumbs';
import styles from './Breadcrumbs.module.scss';

const rootHref = '/';
const rootTitle = 'Home';

export default function Breadcrumbs() {
  const { pathname } = useRouter();
  const { breadcrumbs, title } = getBreadcrumbs(pathname);
  const isRootPage = pathname === rootHref;
  return (
    <nav>
      <Head>
        <title>{title}</title>
      </Head>
      <ol className={styles.breadcrumbs}>
        {!isRootPage && (
          <li className={clsx([styles.breadcrumb_item])}>
            <Link href={rootHref}>{rootTitle || 'Home'}</Link>
          </li>
        )}
        {breadcrumbs.map(({ href, title: label }) => {
          if (!label) return null;
          return (
            <li key={href} className={styles.breadcrumb_item}>
              <Link href={href}>
                <a>{label}</a>
              </Link>
            </li>
          );
        })}
        {title && (
          <li className={clsx([styles.breadcrumb_item, styles.active])}>
            {title}
          </li>
        )}
      </ol>
    </nav>
  );
}
