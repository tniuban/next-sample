/* eslint-disable @next/next/no-img-element */
import React from 'react';
import { Logo } from '@svg';
import Link from 'next/link';
import styles from './Navbar.module.scss';
import { caretoryMocks } from '@mocks/category';
import Badge from '@components/Badge';
import { useSelector } from 'react-redux';
import clsx from 'clsx';

export default function Navbar({ categories = caretoryMocks }) {
  const { cart, userLoggedIn } = useSelector((state) => ({
    cart: state.cart,
    userLoggedIn: state.userLoggedIn,
  }));
  return (
    <div className={styles.root}>
      <div className="container d-flex align-items-center">
        <div className="d-flex align-items-center w-100">
          <Link href="/">
            <div className={styles.logo_container}>
              <Logo width={60} />
              <span className={styles.logo_text}>Human Resources</span>
            </div>
          </Link>
          {categories.map(({ id, thumbnail, name }) => (
            <Link href={`/${id}`} key={id}>
              <div className={styles.category}>
                <img
                  className={styles.category_icon}
                  src={thumbnail}
                  alt={''}
                />
                {name}
              </div>
            </Link>
          ))}
        </div>
        <div className="d-flex justify-content-end align-items-center">
          <Link href="/cart">
            <div>
              <Badge number={cart.totalQuantity} />
            </div>
          </Link>
          <div className={clsx([styles.category, styles.cart_sign_in])}>
            <i className="fa-solid fa-user-astronaut" />
            &nbsp;
            {!userLoggedIn.isLoggedIn
              ? 'Sign in'
              : userLoggedIn.details.profile.firstName}
          </div>
        </div>
      </div>
    </div>
  );
}
