import React from 'react';

const TextArea = ({
  id,
  name,
  value,
  placeholder,
  onChange,
  error,
  label,
  required,
  disabled,
  disabledLabel,
  disabledError,
}) => {
  return (
    <div className="form-group mt-1">
      {!disabledLabel && (
        <label
          data-testid={`${id}-label`}
          className="mb-1 fw-bold form-label"
          htmlFor={id}
        >
          {label}
          {required && label && (
            <span data-testid={`${id}-required`} className="text-danger">
              &nbsp;*
            </span>
          )}
        </label>
      )}
      <textarea
        data-testid={`${id}-textarea`}
        className={`form-control ${error ? 'is-invalid' : ''}`}
        id={id}
        disabled={disabled}
        name={name}
        onChange={onChange}
        value={value}
        placeholder={placeholder}
      />
      {!disabledError &&
        (error ? (
          <small data-testid={`${id}-error`} className="invalid-feedback">
            {error}
          </small>
        ) : (
          <div style={{ height: 25 }} />
        ))}
    </div>
  );
};

export default TextArea;
