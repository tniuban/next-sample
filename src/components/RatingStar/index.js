// import React from 'react';
// import clsx from 'clsx';
// import styles from './RatingStar.module.scss';

// const number = '?'.repeat(5).split('');

// export default function RatingStar({ value: val = 4.4, onChange }) {
//   const [value, setValue] = React.useState(val);
//   const onMouseChange = (_value) => (event) => {
//     event.preventDefault();
//     setValue(_value);
//   };
//   return (
//     <div
//       className={styles.container}
//       {...(onChange && { onMouseLeave: onMouseChange(val) })}
//     >
//       {number.map((_, idx) => (
//         <div
//           key={idx}
//           className={clsx([
//             styles.rating,
//             { [styles.cover_rating_hover]: onChange },
//           ])}
//           {...(onChange && {
//             onMouseEnter: onMouseChange(idx + 1),
//             onClick: (event) => {
//               event.preventDefault();
//               onChange(idx + 1);
//             },
//           })}
//         >
//           <div
//             className={styles.cover_rating}
//             style={{
//               '--width-star':
//                 value <= idx + 1
//                   ? `${73 * Math.max(0, -idx + value) + 11}%`
//                   : '100%',
//             }}
//           />
//         </div>
//       ))}
//     </div>
//   );
// }

import React, { useEffect } from 'react';
import clsx from 'clsx';
import styles from './RatingStar.module.scss';

const number = '?'.repeat(5).split('');

export default function RatingStar({ value: val = 4.4, onChange }) {
  const [value, setValue] = React.useState(val);
  const onMouseChange = (_value) => (event) => {
    event.preventDefault();
    setValue(_value);
  };

  useEffect(() => {
    setValue(val);
  }, [val]);

  return (
    <div
      className={styles.container}
      {...(onChange && { onMouseLeave: onMouseChange(val) })}
    >
      {number.map((_, idx) => (
        <div
          key={idx}
          className={clsx([
            styles.rating,
            { [styles.cover_rating_hover]: onChange },
          ])}
          {...(onChange && {
            onMouseEnter: onMouseChange(idx + 1),
            onClick: (event) => {
              event.preventDefault();
              onChange(idx + 1);
            },
          })}
        >
          <div
            className={styles.cover_rating}
            style={{
              width:
                value <= idx + 1
                  ? `${73 * Math.max(0, -idx + value) + 11}%`
                  : '100%',
            }}
          />
        </div>
      ))}
    </div>
  );
}
