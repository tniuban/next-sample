/* eslint-disable @next/next/no-img-element */
import React from 'react';
import { toggleStatus } from '@redux/actions/sidebar';
import { useDispatch, useSelector } from 'react-redux';
import Breadcrumbs from '@components/Breadcrumbs';
import styles from './Header.module.scss';
import Badge from '@components/Badge';
import Link from 'next/link';
import { poster } from '@services';
import Router from 'next/router';
import { logout } from '@redux/actions/userLoggedIn';

export default function Header() {
  const dispatch = useDispatch();
  const { isOpen, user } = useSelector(({ userLoggedIn, sidebar }) => ({
    isOpen: sidebar.isOpen,
    user: userLoggedIn,
  }));
  const handleToggle = (event) => {
    event.preventDefault();
    dispatch(toggleStatus());
  };
  const { totalQuantity } = useSelector((state) => state.cart);
  const handleLogOut = async (event) => {
    event.preventDefault();
    await poster('/api/auth/logout');
    dispatch(logout());
    Router.reload();
  };
  return (
    <div className={styles.header}>
      <div className="d-flex align-items-center justify-content-space-between">
        <button
          type="button"
          onClick={handleToggle}
          className="btn btn-outline-primary"
        >
          <i className={`fa-solid fa-angles-${isOpen ? 'left' : 'right'}`} />
        </button>
        <div className="px-4">
          <Breadcrumbs />
          {/* <div className='d-flex justify-content-between align-items-center'>
            <div className='fw-bold fs-3'>
              {title} {subTitle ? `: ${subTitle}` : ''}
            </div>
          </div> */}
        </div>
      </div>
      <div className="d-flex justify-content-end align-items-center">
        <Link href="/cart">
          <button type="button" className="btn btn-outline-dark">
            <Badge number={totalQuantity} />
          </button>
        </Link>
        {user.isLoggedIn && (
          <img
            onClick={handleLogOut}
            className={styles.avatar}
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQD3TDQBB-_F1sfu-gElz73vtUAdlOdLerHDw&usqp=CAU"
            alt="avatar"
          />
        )}
      </div>
    </div>
  );
}
