import React from 'react';
import styles from './Loader.module.scss';

export default function Loader() {
  return (
    <div className={styles.loader_container}>
      {/* <div className={styles.loader_3}>
        <div className={styles.circle} />
        <div className={styles.circle} />
        <div className={styles.circle} />
        <div className={styles.circle} />
        <div className={styles.circle} />
      </div> */}
      <div className={styles.spinner}>
        <div />
        <div />
        <div />
        <div />
        <div />
      </div>
    </div>
  );
}
