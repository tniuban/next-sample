import clsx from 'clsx';
import React from 'react';
import styles from './Layout.module.scss';

export default function LayoutLogin({ children }) {
  return (
    <div className="d-flex">
      <div
        className={clsx(
          'col-lg-6 col-md-6 col-sm-6  d-none d-sm-block',
          styles.background
        )}
      ></div>
      <div className={clsx('col-lg-6 col-sm-6', styles.content)}>
        <div className="col-10 mx-auto">{children}</div>
        <div className={styles.footer}>
          ©{new Date().getFullYear()} - Copyright by Human Resources
        </div>
      </div>
    </div>
  );
}
