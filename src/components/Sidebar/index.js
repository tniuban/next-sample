import React, { useCallback } from 'react';
import Link from 'next/link';
import clsx from 'clsx';
import styles from './Sidebar.module.scss';
import { useDispatch } from 'react-redux';
import { toggleStatus } from '@redux/actions/sidebar';
import sidebarConfig from '@constants/sidebarConfig';
import { useRouter } from 'next/router';
import { Logo } from '@svg';

const Sidebar = ({ isOpen }) => {
  const { pathname } = useRouter();
  const dispatch = useDispatch();
  const handleToggle = useCallback(() => {
    dispatch(toggleStatus());
  }, [dispatch]);

  return (
    <div className={clsx([styles.sidebar, { [styles.is_open]: isOpen }])}>
      <div className={styles.sidebar_sticky}>
        <div>
          <div className={styles.sidebar_header}>
            <div className={clsx([styles.nav])}>
              <div className={styles.nav_icon}>
                <Logo width={35} />
              </div>
              <div>
                <span className='fs-3'>HươngCute</span>
              </div>
            </div>
            <button
              variant='link'
              className={styles.close_button}
              onClick={handleToggle}
            >
              <i className='fa-solid fa-align-left' />
            </button>
          </div>

          <div className='flex-column pt-2'>
            {sidebarConfig.map(({ href, title, icon }) => (
              <Link href={href} key={href}>
                <div
                  className={clsx([
                    styles.nav_item,
                    styles.nav,
                    { [styles.active]: pathname === href },
                  ])}
                >
                  <div className={styles.nav_icon}>{icon}</div>
                  <div className={styles.nav_text}>
                    <span>{title}</span>
                  </div>
                </div>
              </Link>
            ))}
          </div>
        </div>
        <div className={styles.version_container}>
          <p>v 0.1.02</p>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
