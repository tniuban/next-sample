import React from 'react';
import { poster } from '@services';
import useSWR from 'swr';

const getOption = (_data) => {
  if (!Array.isArray(_data)) return [];
  return _data.map(({ id: value, name: label }) => ({ value, label }));
};

const Select = (props) => {
  const {
    label,
    required,
    error: errMsg,
    id,
    api,
    keyData,
    query,
    value,
    name,
    disabled,
    onChange,
  } = props;

  const { data: { data: resp = {} } = {}, error: errFetcher } = useSWR(
    [api, query],
    (url, query) => poster(url, { limit: -1, ...query })
  );
  const error = errFetcher || errMsg;
  const option = getOption(resp.data?.[keyData]);

  return (
    <div className="form-group mt-1">
      <label
        data-testid={`${id}-label`}
        className="mb-1 fw-bold form-label"
        htmlFor={id}
      >
        {label}
        {required && label && (
          <span data-testid={`${id}-required`} className="text-danger">
            &nbsp;*
          </span>
        )}
      </label>
      <select
        data-testid={`${id}-select`}
        disabled={disabled}
        className={`form-select ${error ? 'is-invalid' : ''}`}
        id={id}
        name={name}
        onChange={onChange}
        value={value}
      >
        <option data-testid={`${id}-option`} disabled value="">
          Choose...
        </option>
        {option.map(({ value, label }) => (
          <option
            data-testid={`${id}-option`}
            value={value}
            key={`${value}_${id}`}
          >
            {label}
          </option>
        ))}
      </select>
      {error ? (
        <small data-testid={`${id}-error`} className="invalid-feedback">
          {error}
        </small>
      ) : (
        <div style={{ height: 25 }} />
      )}
    </div>
  );
};

export default Select;
