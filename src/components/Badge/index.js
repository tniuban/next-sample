import React from 'react';
import styles from './Badge.module.scss';

const getNumber = (_number) => {
  if (isNaN(_number) || _number === 0) return null;
  if (_number > 9) return '+9';
  return _number;
};

export default function Badge({ icon, number }) {
  const num = getNumber(number);
  return (
    <div className={styles.box_badge}>
      {icon || <i className="fa-solid fa-cart-shopping" />}
      {num && <span className={styles.badge_number}>{num}</span>}
    </div>
  );
}
