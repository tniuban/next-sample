import React from 'react';

const PrimaryInput = ({
  id,
  name,
  value,
  type,
  placeholder,
  onChange,
  error,
  label,
  required,
  disabled,
  disabledLabel,
  disabledError,
}) => {
  return (
    <div className="form-group mt-1">
      {!disabledLabel && (
        <label
          data-testid={`${id}-label`}
          className="mb-1 fw-bold form-label"
          htmlFor={id}
        >
          {label}
          {required && label && (
            <span data-testid={`${id}-required`} className="text-danger">
              &nbsp;*
            </span>
          )}
        </label>
      )}
      <input
        data-testid={`${id}-input`}
        type={type}
        className={`form-control ${error ? 'is-invalid' : ''}`}
        id={id}
        disabled={disabled}
        name={name}
        onChange={onChange}
        value={value}
        placeholder={placeholder}
      />
      {!disabledError &&
        (error ? (
          <small data-testid={`${id}-error`} className="invalid-feedback">
            {error}
          </small>
        ) : (
          <div style={{ height: 25 }} />
        ))}
    </div>
  );
};

export default PrimaryInput;
