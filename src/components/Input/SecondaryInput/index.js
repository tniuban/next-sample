import clsx from 'clsx';
import React from 'react';
import styles from './SecondaryInput.module.scss';

const SecondaryInput = ({
  id,
  name,
  value,
  type,
  placeholder,
  onChange,
  error,
  required,
  disabled,
}) => {
  return (
    <div className="form-group mt-1">
      <input
        data-testid={`${id}-input`}
        type={type}
        className={clsx([
          { ['is-invalid']: error },
          'form-control',
          styles.input,
        ])}
        id={id}
        disabled={disabled}
        name={name}
        onChange={onChange}
        value={value}
        placeholder={placeholder + (required ? ' *' : '')}
      />
      {error ? (
        <span data-testid={`${id}-error`} className="invalid-feedback fs-6">
          {error}
        </span>
      ) : (
        <div style={{ height: 25 }} />
      )}
    </div>
  );
};

export default SecondaryInput;
