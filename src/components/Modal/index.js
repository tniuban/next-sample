import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import clsx from "clsx";
import styles from "./Modal.module.scss";

export default function Modal({ onClose, open, title, children }) {
  const [_open, setOpen] = useState(open);

  useEffect(() => {
    let interval;
    if (open) {
      setOpen(true);
    } else {
      interval = setInterval(() => {
        setOpen(false);
      }, 500);
    }
    return () => {
      clearInterval(interval);
    };
  }, [open]);

  if (!_open) return <></>;
  return ReactDOM.createPortal(
    <div
      data-testid="modal-backdrop"
      className={clsx(["modal", styles.modal, { [styles.show]: _open }])}
      onClick={onClose}
    >
      <div
        data-testid="modal-container"
        className={clsx([
          "modal-dialog",
          { [styles.appear]: open },
          { [styles.disapper]: !open },
        ])}
        onClick={(event) => event.stopPropagation()}
      >
        <div className="modal-content">
          <div className="modal-header">
            {title && (
              <h5 data-testid="modal-title" className="modal-title">
                {title}
              </h5>
            )}
            <button
              data-testid="close-modal-button"
              type="button"
              className="btn-close"
              onClick={onClose}
            />
          </div>
          {children}
        </div>
      </div>
    </div>,
    document.querySelector("body")
  );
}
