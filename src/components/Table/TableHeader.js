import React from 'react';

const TableHeader = ({ columns = [], title }) => (
  <thead
    data-testid='table-header'
    className='thead-light bg-primary text-light'
  >
    {title && (
      <tr data-testid='table-header-title-row' className='bg-white'>
        <td
          align='center'
          data-testid='table-header-title-cell'
          colSpan={columns.length}
        >
          <h2
            data-testid='table-header-title-text'
            className='text-center text-uppercase py-3 fw-bold text-dark'
          >
            {title}
          </h2>
        </td>
      </tr>
    )}
    <tr data-testid='table-header-row'>
      {columns.map(({ id, label, props }, idx) => (
        <th
          data-testid={`table-header-cell-${idx}`}
          scope='col'
          {...props}
          key={`${id}_${label}`}
        >
          {label}
        </th>
      ))}
    </tr>
  </thead>
);

export default TableHeader;
