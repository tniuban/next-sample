import Error from '@components/Errror';
import Loading from '@components/Loading';
import React from "react";
import TableRow from "./TableRow";

export default function TableBody({ loading, error, data, columns }) {
  if (loading)
    return (
      <tbody>
        <tr className="text-center">
          <td colSpan={columns.length}>
            <Loading />
          </td>
        </tr>
      </tbody>
    );
  if (error || !Array.isArray(data))
    return (
      <tbody>
        <tr className="text-center">
          <td colSpan={columns.length}>
            <Error message={error || "Something went wrong"} />
          </td>
        </tr>
      </tbody>
    );
  if (!data.length)
    return (
      <tbody>
        <tr className="text-center">
          <td colSpan={columns.length}>
            <span>(Empty data)</span>
          </td>
        </tr>
      </tbody>
    );
  return (
    <tbody>
      {data.map((customer, index) => (
        <TableRow
          key={`${customer.id}_${index}`}
          columns={columns}
          data={{ ...customer, index }}
        />
      ))}
    </tbody>
  );
}