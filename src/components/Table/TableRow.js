import React from 'react';

const TableRow = ({ columns, data }) => (
  <tr data-testid='table-row'>
    {columns.map(({ id, props, renderCell }, idx) => (
      <td data-testid={`table-row-cell-${idx}`} {...props} key={`${id}_${idx}`}>
        {renderCell ? renderCell(data[id], data) : data[id]}
      </td>
    ))}
  </tr>
);

export default TableRow;
