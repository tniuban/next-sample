import toast from './toastSlice';
import sidebar from './sidebarSlice';
import header from './headerSlice';
import cart from './cartSlice';
import userLoggedIn from './userLoggedInSlice';

const reducer = {
  toast,
  sidebar,
  header,
  cart,
  userLoggedIn,
};
export default reducer;
