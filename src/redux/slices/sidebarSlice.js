import { createSlice } from '@reduxjs/toolkit';

const initialState = { isOpen: true };

export const sidebarSlice = createSlice({
  name: 'sidebar',
  initialState,
  reducers: {
    toggleStatus: (state) => {
      state.isOpen = !state.isOpen;
    },
  },
});

export default sidebarSlice.reducer;
