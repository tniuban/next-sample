import { toastBackground, toastTitle } from '@constants/toast';
import { createSlice } from '@reduxjs/toolkit';
const initialState = [];

export const toastSlice = createSlice({
  name: 'toast',
  initialState,
  reducers: {
    addToast: (state, { payload }) => {
      const toast = {
        id: (state?.[0]?.id || 0) + 1,
        title: payload?.title || toastTitle[payload?.type],
        description: payload?.description || '',
        backgroundColor: toastBackground[payload?.type],
      };
      state.push(toast);
    },

    deleteToast: (state, { payload: idx }) => {
      const index = state.findIndex(({ id }) => id === idx);
      state = state.splice(index, 1);
    },
  },
});

export default toastSlice.reducer;
