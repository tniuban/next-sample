import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isLoggedIn: false,
  details: {},
};

export const userLoggedInSlice = createSlice({
  name: 'userLoggedIn',
  initialState,
  reducers: {
    setUserLoggedInDetails: (state, { payload: {isLoggedIn, details} }) => {
      if (isLoggedIn && details?.id) {
        state.isLoggedIn = true;
        state.details = details;
      }
    },
    logout: (state) => {
      state.isLoggedIn = initialState.isLoggedIn;
      state.details = initialState.details;
    },
  },
});

export default userLoggedInSlice.reducer;
