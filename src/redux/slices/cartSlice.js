import { createSlice } from '@reduxjs/toolkit';
const initialState = { items: [], totalQuantity: 0, totalPrice: 0 };

export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  // : {
  //   items: [
  //     {
  //       id: 3,
  //       name: 'iPhone 13 Pro Max',
  //       thumbnail:
  //         'https://cdn.tgdd.vn/Products/Images/42/230529/TimerThumb/iphone-13-pro-max-(8).jpg',
  //       quantity: 3,
  //       price: 33990000,
  //       discountPrice: 28190000,
  //     },
  //     {
  //       id: 1,
  //       name: 'iPhone 13 Pro Max',
  //       thumbnail:
  //         'https://cdn.tgdd.vn/Products/Images/42/230529/TimerThumb/iphone-13-pro-max-(8).jpg',
  //       quantity: 3,
  //       price: 33990000,
  //       discountPrice: 28190000,
  //     },
  //   ],
  // totalQuantity: 6,
  // totalPrice: 84570000 * 2,
  // },
  reducers: {
    addProductToCart: (
      state,
      { payload: { id, name, thumbnail, quantity = 1, price, discountPrice } }
    ) => {
      const idx = state.items.findIndex((element) => element.id === id);
      if (idx === -1) {
        state.items.push({
          id,
          name,
          thumbnail,
          quantity,
          price,
          discountPrice,
        });
      } else {
        state.items[idx].quantity += quantity;
      }
      state.totalPrice += discountPrice * quantity;
      state.totalQuantity += quantity;
    },

    changeProductQuantityInCart: (state, { payload: { id, quantity = 1 } }) => {
      const idx = state.items.findIndex((element) => element.id === id);
      state.totalQuantity += quantity - state.items[idx].quantity;
      state.items[idx].quantity = quantity;
    },

    removeProductFromCart: (state, { payload: idx }) => {
      const index = state.items.findIndex(({ id }) => id === idx);
      state.totalPrice -=
        state.items[index].discountPrice * state.items[index].quantity;
      state.totalQuantity -= state.items[index].quantity;
      state.items.splice(index, 1);
    },
  },
});

export default cartSlice.reducer;
