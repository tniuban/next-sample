import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  title: 'Home',
  breadcrumbs: [],
  rootTitle: 'Home',
};

export const headerSlice = createSlice({
  name: 'header',
  initialState,
  reducers: {
    setHeaderDetails: (state, { payload: { title, breadcrumbs = [] } }) => {
      state.breadcrumbs = breadcrumbs;
      state.title = title;
    },
    setRootTitle: (state, { payload }) => {
      state.rootTitle = payload;
    },
  },
});

export default headerSlice.reducer;
