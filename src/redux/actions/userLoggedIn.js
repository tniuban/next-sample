import { userLoggedInSlice } from '../slices/userLoggedInSlice';

export const { setUserLoggedInDetails, logout } = userLoggedInSlice.actions;
