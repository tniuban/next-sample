import { sidebarSlice } from '../slices/sidebarSlice';

export const { toggleStatus } = sidebarSlice.actions;
