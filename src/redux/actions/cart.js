import { cartSlice } from '../slices/cartSlice';

export const {
  addProductToCart,
  changeProductQuantityInCart,
  removeProductFromCart,
} = cartSlice.actions;
