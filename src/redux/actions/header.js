import { headerSlice } from '../slices/headerSlice';

export const { setHeaderDetails, setRootTitle } = headerSlice.actions;
