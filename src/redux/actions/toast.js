import { toastSlice } from "../slices/toastSlice";

export const { addToast, deleteToast } = toastSlice.actions;

