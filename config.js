/* eslint-disable import/no-anonymous-default-export */

export default {
  appReleaseVersion: '0.2.29',
  baseApiUrl: process.env.API_URL,
  domain: process.env.DOMAIN,
  sessionName: process.env.SESSION_NAME,
  sessionPassword: process.env.SESSION_PASSWORD,
};
